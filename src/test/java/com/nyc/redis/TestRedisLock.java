package com.nyc.redis;

import com.nyc.base.component.lock.LockGenerator;
import com.nyc.base.util.SpringContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;


@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class TestRedisLock {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Test
    public void testRedisLock() throws Exception {
        String lockName = "12345";
        String lockValue = UUID.randomUUID().toString();
        String redisTimeOut = "50000";

        /*DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>(LuaScript.LUCK_LUA, Long.class);
        Object result = redisTemplate.execute(redisScript, Collections.singletonList(lockName), redisTimeOut, "0");
        log.info("redisTemplate execute:{}", JSON.toJSONString(result));*/

        LockGenerator lockGenerator = SpringContextUtil.getBean(LockGenerator.class);
        // austinExecutor "asyncExecutor", "myAsyncExecutor"
        Executor executor = SpringContextUtil.getBean("asyncExecutor", Executor.class);
        Lock lock1 = lockGenerator.getLock(lockName+System.currentTimeMillis());
        lock1.lock();
        Thread.sleep(300*1000);
        log.info("redisTemplate execute:{}", lockValue);
        /*executor.execute(() -> {
            Lock lock = lockGenerator.getLock(lockName);
            lock.lock();
        });
        executor.execute(() -> {
            Lock lock = lockGenerator.getLock(lockName);
            lock.lock();
        });
        executor.execute(() -> {
            Lock lock = lockGenerator.getLock(lockName);
            lock.lock();
        });*/
    }

    @Test
    public void A() throws InterruptedException {
        //插入单条数据
        redisTemplate.opsForValue().set("key1", "我是新信息");
        System.out.println(redisTemplate.opsForValue().get("key1"));
        //插入单条数据（存在有效期）
        System.out.println("-----------------");
        redisTemplate.opsForValue().set("key2", "这是一条会过期的信息", 1, TimeUnit.SECONDS);//向redis里存入数据和设置缓存时间
        System.out.println(redisTemplate.hasKey("key2"));
        // 检查key是否存在，返回boolean值
        System.out.println(redisTemplate.opsForValue().get("key2"));
        Thread.sleep(2000);
        System.out.println(redisTemplate.hasKey("key2"));//检查key是否存在，返回boolean值  
        System.out.println(redisTemplate.opsForValue().get("key2"));
        System.out.println("-----------------");
        redisTemplate.opsForValue().set("key2", "我是新信息");
    }

    @Test
    public void deleteKey() throws InterruptedException {
        redisTemplate.delete("key1");
    }

}