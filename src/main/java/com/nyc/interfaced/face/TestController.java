package com.nyc.interfaced.face;

import com.nyc.base.util.SpringContextUtil;
import com.nyc.event.EventType;
import com.nyc.event.SpringEvent;
import com.nyc.event.activity.CreateActivityEventBody;
import com.nyc.event.activity.DelActivityEventBody;
import com.nyc.event.publish.EventPublishByApplicationContext;
import com.nyc.event.publish.PublishEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * TestEvent
 *
 * @Description: TestEvent
 * @Author: nyc
 * @Date: 2023/3/8
 */
@RestController
public class TestController {
    @Autowired
    private EventPublishByApplicationContext eventPublishByApplicationContext;

    @PostMapping("testPublishEvent")
    public List<SpringEvent> test() {
        List<SpringEvent> testEventList = new ArrayList<>();
        CreateActivityEventBody eventBody = new CreateActivityEventBody("name", "code");
        SpringEvent testEvent = new SpringEvent(eventBody, EventType.ADD_ACTIVITY);
        testEventList.add(testEvent);
        eventPublishByApplicationContext.publishEventByApplicationContext(testEvent);
        eventPublishByApplicationContext.publishEventByApplicationEventPublisher(testEvent);
        PublishEvent.publishEvent(testEvent);
        SpringContextUtil.getApplicationContext().publishEvent(testEvent);



        CreateActivityEventBody createActivityEventBody = new CreateActivityEventBody("name", "code");
        createActivityEventBody.setActivityCode("activityCode");
        createActivityEventBody.setActivityName("activityName");
        createActivityEventBody.setCreateTime("11111");
        SpringEvent createActTestEvent = new SpringEvent(createActivityEventBody, EventType.ADD_ACTIVITY);
        testEventList.add(createActTestEvent);
        eventPublishByApplicationContext.publishEventByApplicationContext(createActTestEvent);
        eventPublishByApplicationContext.publishEventByApplicationEventPublisher(createActTestEvent);
        PublishEvent.publishEvent(createActTestEvent);
        SpringContextUtil.getApplicationContext().publishEvent(createActTestEvent);


        DelActivityEventBody delActivityEventBody = new DelActivityEventBody("name", "code");
        delActivityEventBody.setActivityCode("activityCode");
        delActivityEventBody.setActivityName("activityName");
        SpringEvent delActTestEvent = new SpringEvent(delActivityEventBody, EventType.DEL_ACTIVITY);
        testEventList.add(delActTestEvent);
        eventPublishByApplicationContext.publishEventByApplicationContext(delActTestEvent);
        eventPublishByApplicationContext.publishEventByApplicationEventPublisher(delActTestEvent);
        PublishEvent.publishEvent(delActTestEvent);
        SpringContextUtil.getApplicationContext().publishEvent(delActTestEvent);
        return testEventList;
    }
}
