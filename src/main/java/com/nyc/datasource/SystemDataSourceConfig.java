package com.nyc.datasource;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.core.JdbcTemplate;
 
@Configuration
@MapperScan(basePackages = {"com.nyc.mapper.system"},
        sqlSessionFactoryRef = "sqlSessionFactorySystem")
public class SystemDataSourceConfig {

   @Primary
   @Bean(name = "systemDataSource")
   @ConfigurationProperties(prefix = "spring.datasource.system")
   public DataSource systemDataSource() {
      return DataSourceBuilder.create().build();
   }

   @Bean(name = "sqlSessionFactorySystem")
   public SqlSessionFactory sqlSessionFactorySystem() throws Exception {
      SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
      factoryBean.setDataSource(systemDataSource());
      factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver()
              .getResources("classpath:mapper/system/*.xml"));
      return factoryBean.getObject();
   }

   @Bean
   public SqlSessionTemplate sqlSessionTemplateSystem() throws Exception {
      return new SqlSessionTemplate(sqlSessionFactorySystem());
   }


   @Bean(name = "systemJdbcTemplate")
   @Autowired
   public JdbcTemplate systemJdbcTemplate(@Qualifier("systemDataSource") DataSource dataSource) {
      return new JdbcTemplate(dataSource);
   }
}