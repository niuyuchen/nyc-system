package com.nyc.datasource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * SystemDataSource
 *
 * @Description: SystemDataSource
 * @Author: nyc
 * @Date: 2023/3/10
 */
@Configuration
@MapperScan(basePackages = {"com.nyc.mapper.work"},
        sqlSessionFactoryRef = "sqlSessionFactoryWork")
public class WorkDataSourceConfig {

    @Bean(name = "workDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.work")
    public DataSource workDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "sqlSessionFactoryWork")
    public SqlSessionFactory sqlSessionFactoryWork() throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(workDataSource());
        factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources("classpath:mapper/work/*.xml"));
        return factoryBean.getObject();
    }

    @Bean
    public SqlSessionTemplate sqlSessionTemplateWork() throws Exception {
        return new SqlSessionTemplate(sqlSessionFactoryWork());
    }

    @Bean(name = "workJdbcTemplate")
    @Autowired
    public JdbcTemplate workJdbcTemplate(@Qualifier("workDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

/*
    作者：蛮三刀酱
    链接：https://juejin.cn/post/6844903957186232327
    来源：稀土掘金
    著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
这一篇写的非常好
https://blog.csdn.net/xc9711/article/details/124186473?utm_medium=distribute.pc_feed_404.none-task-blog-2~default~BlogCommendFromBaidu~Rate-3-124186473-blog-null.pc_404_mixedpudn&depth_1-utm_source=distribute.pc_feed_404.none-task-blog-2~default~BlogCommendFromBaidu~Rate-3-124186473-blog-null.pc_404_mixedpud

    */

}