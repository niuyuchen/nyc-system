package com.nyc.event.handler;

import com.nyc.event.SpringEvent;

public interface IEventHandler {
    void handle(SpringEvent testEvent);
}
