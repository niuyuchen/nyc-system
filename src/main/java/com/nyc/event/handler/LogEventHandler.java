package com.nyc.event.handler;

import com.alibaba.fastjson.JSON;
import com.nyc.event.EventType;
import com.nyc.event.SpringEvent;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;

/**
 * LogEventHandler
 *
 * @Description: LogEventHandler
 * @Author: nyc
 * @Date: 2023/3/9
 */
@Log4j2
public class LogEventHandler implements IEventHandler {
    private static final List<EventType> eventTypeList = new ArrayList<>();
    static {
        eventTypeList.add(EventType.ADD_LOG);
        eventTypeList.add(EventType.SAVE_LOG);
    }
    @Override
    public void handle(SpringEvent springEvent) {
        if (!eventTypeList.contains(springEvent.getEventType())){
            log.info("LogEventHandler 不处理 类型：{}, body:{}", JSON.toJSONString(springEvent.getEventType()),JSON.toJSONString(springEvent.getEventBody()));
            return;
        }
        log.info("LogEventHandler 处理：{}", JSON.toJSONString(springEvent));
    }
}
