package com.nyc.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * EventBody
 *
 * @Description: EventBody
 * @Author: nyc
 * @Date: 2023/3/8
 */
@Getter
@Setter
public class EventBody {
}
