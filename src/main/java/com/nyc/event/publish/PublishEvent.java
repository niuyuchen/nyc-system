package com.nyc.event.publish;

import com.nyc.event.SpringEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

@Component
public class PublishEvent implements ApplicationEventPublisherAware {
    public static ApplicationEventPublisher eventPublisher = null;
    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        buildApplicationEventPublisher(applicationEventPublisher);
    }

    private static void buildApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        eventPublisher = applicationEventPublisher;
    }

    public static void publishEvent(SpringEvent testEvent){
        eventPublisher.publishEvent(testEvent);
    }
}