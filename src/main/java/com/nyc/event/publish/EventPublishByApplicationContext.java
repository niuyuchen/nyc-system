package com.nyc.event.publish;

import com.nyc.event.SpringEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * EventPublishByApplicationContext
 *
 * @Description: EventPublishByApplicationContext
 * @Author: nyc
 * @Date: 2023/3/8
 */
@Component
public class EventPublishByApplicationContext {
    //发布方式1 直接注入ApplicationContext:
    @Resource
    private ApplicationContext applicationContext;

    public void publishEventByApplicationContext(SpringEvent testEvent) {
        applicationContext.publishEvent(testEvent);
    }

    //发布方式2    直接注入ApplicationEventPublisher:
    @Resource
    private ApplicationEventPublisher applicationEventPublisher;

    public void publishEventByApplicationEventPublisher(SpringEvent testEvent) {
        applicationEventPublisher.publishEvent(testEvent);
    }
}
