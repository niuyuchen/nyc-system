package com.nyc.event.listener;

import com.alibaba.fastjson.JSON;
import com.nyc.event.SpringEvent;
import com.nyc.event.handler.ActivityEventHandler;
import com.nyc.event.handler.IEventHandler;
import com.nyc.event.handler.LogEventHandler;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Log4j2
@Component
public class AEventListener implements ApplicationListener<SpringEvent> {
    private final static List<IEventHandler> eventHandlerList = new ArrayList<>();
    static {
        eventHandlerList.add(new ActivityEventHandler());
        eventHandlerList.add(new LogEventHandler());
    }

    @Override
    public void onApplicationEvent(SpringEvent event) {
        //逻辑处理
        log.info(JSON.toJSONString(event));
        for (IEventHandler eventHandler : eventHandlerList) {
            eventHandler.handle(event);
        }
        log.info("end");
    }
}