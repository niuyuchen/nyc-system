package com.nyc.event;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * TestEvent
 *
 * @Description: TestEvent
 * @Author: nyc
 * @Date: 2023/3/8
 */
@Getter
@Setter
public class SpringEvent extends ApplicationEvent {
    private EventBody eventBody;
    private EventType eventType;
    public SpringEvent(EventBody eventBody, EventType eventType) {
        super(eventBody);
        this.eventType = eventType;
    }

    public EventBody getEventBody() {
        return (EventBody) getSource();
    }
}
