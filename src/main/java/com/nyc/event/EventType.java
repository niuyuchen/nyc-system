package com.nyc.event;

public enum EventType {
    ADD_ACTIVITY,
    DEL_ACTIVITY,
    CREATE_ACTIVITY,

    ADD_LOG,
    SAVE_LOG,
    ;
}
