package com.nyc.event.activity;

import com.nyc.event.EventBody;
import lombok.Getter;
import lombok.Setter;

/**
 * EventBody
 *
 * @Description: EventBody
 * @Author: nyc
 * @Date: 2023/3/8
 */
@Getter
@Setter
public abstract class ActivityEventBody extends EventBody {
    private String name;
    private String code;

    public ActivityEventBody(String name, String code) {
        this.code = code;
        this.name = name;
    }
}
