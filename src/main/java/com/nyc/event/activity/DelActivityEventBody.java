package com.nyc.event.activity;

import com.nyc.event.EventBody;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * EventBody
 *
 * @Description: EventBody
 * @Author: nyc
 * @Date: 2023/3/8
 */
@Getter
@Setter
public class DelActivityEventBody extends ActivityEventBody {
    private String activityCode;
    private String activityName;

    public DelActivityEventBody(String name, String code) {
        super(name, code);
    }
}
