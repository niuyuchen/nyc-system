package com.nyc.event.activity;

import com.nyc.event.EventBody;
import lombok.Getter;
import lombok.Setter;

/**
 * EventBody
 *
 * @Description: EventBody
 * @Author: nyc
 * @Date: 2023/3/8
 */
@Getter
@Setter
public class CreateActivityEventBody extends ActivityEventBody {
    private String activityCode;
    private String activityName;
    private String createTime;

    public CreateActivityEventBody(String name, String code) {
        super(name, code);
    }
}
