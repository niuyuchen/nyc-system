package com.nyc.work.task.model;

import lombok.Getter;
import lombok.Setter;

/**
 * TaskParam
 *
 * @Description: TaskParam
 * @Author: nyc
 * @Date: 2023/3/7
 */
@Getter
@Setter
public class TaskParam {
    private Long id;
    private String type;
    private String name;
    private String code;
}
