package com.nyc.work.task.model;

import lombok.Getter;
import lombok.Setter;

/**
 * TaskVo
 *
 * @Description: TaskVo
 * @Author: nyc
 * @Date: 2023/3/7
 */
@Getter
@Setter
public class TaskVo {
    private Long id;
    private String type;
    private String name;
    private String code;
}
