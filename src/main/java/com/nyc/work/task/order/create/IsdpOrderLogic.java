package com.nyc.work.task.order.create;

import com.nyc.work.task.order.build.SingleTaskOrderLogic;
import com.nyc.work.task.order.databuilder.IhubTaskContextBuilder;
import com.nyc.work.task.order.extend.PythonExtend;
import com.nyc.work.task.order.verify.OrderCommonVerify;
import com.nyc.work.task.order.verify.OrderNameVerify;

/**
 * IsdpOrderLogic
 *
 * @Description: IsdpOrderLogic
 * @Author: nyc
 * @Date: 2023/3/7
 */
public class IsdpOrderLogic implements ICreateSingleTaskOrder {

    private final SingleTaskOrderLogic singleTaskOrderLogic;

    public IsdpOrderLogic() {
        singleTaskOrderLogic = new SingleTaskOrderLogic();
        singleTaskOrderLogic.registerVerifier(new OrderCommonVerify());
        singleTaskOrderLogic.registerVerifier(new OrderNameVerify());
        singleTaskOrderLogic.registerExtendLogic(new PythonExtend());
        singleTaskOrderLogic.setTaskDataContextBuilder(new IhubTaskContextBuilder());
    }

    @Override
    public void createTaskOrder() {
        singleTaskOrderLogic.createOrder();
    }
}
