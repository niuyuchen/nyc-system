package com.nyc.work.task.order.databuilder;

import com.nyc.work.task.model.TaskVo;

public interface ITaskContextBuilder {
    TaskVo buildTaskContext();
}
