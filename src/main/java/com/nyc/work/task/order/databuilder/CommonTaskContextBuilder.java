package com.nyc.work.task.order.databuilder;

import com.nyc.work.task.model.TaskVo;

/**
 * CommonTaskDataContextBuilder
 *
 * @Description: CommonTaskDataContextBuilder
 * @Author: nyc
 * @Date: 2023/3/8
 */
public abstract class CommonTaskContextBuilder implements ITaskContextBuilder {
    @Override
    public TaskVo buildTaskContext() {
        TaskVo taskVo = buildNormalTaskData();
        taskVo.setCode("CommonTaskDataContextBuilder");
        return taskVo;
    }

    protected abstract TaskVo buildNormalTaskData();
}
