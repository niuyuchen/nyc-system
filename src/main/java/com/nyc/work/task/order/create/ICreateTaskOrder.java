package com.nyc.work.task.order.create;

/**
 * ICreateTaskOrder
 *
 * @author nyc
 */
public interface ICreateTaskOrder {

    void createTaskOrder();
}
