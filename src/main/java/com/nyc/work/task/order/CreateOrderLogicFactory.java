package com.nyc.work.task.order;

import com.nyc.work.task.order.create.*;

import java.util.HashMap;
import java.util.Map;

/**
 * CreateOederLogicFactory
 *
 * @Description: CreateOederLogicFactory
 * @Author: nyc
 * @Date: 2023/3/7
 */
public class CreateOrderLogicFactory {
    private static final Map<String, Class<? extends ICreateTaskOrder>>
            createOrderLogicMap = new HashMap<>();

    static {
        createOrderLogicMap.put("1", NormalOrderLogic.class);
        createOrderLogicMap.put("2", IsdpOrderLogic.class);
        createOrderLogicMap.put("3", IhubOrderLogic.class);
        createOrderLogicMap.put("4", AutoIhubOrderLogic.class);
    }

    public static ICreateTaskOrder getCreateOrderLogic(String type) {
        try {
            if (createOrderLogicMap.containsKey(type)) {
                return createOrderLogicMap.get(type).newInstance();
            }
        } catch (Exception e) {
            return new NormalOrderLogic();
        }
        return new NormalOrderLogic();
    }
}
