package com.nyc.work.task.order.build;

import com.nyc.work.task.model.TaskVo;
import com.nyc.work.task.order.databuilder.ITaskContextBuilder;
import com.nyc.work.task.order.extend.ICreateOrderExtend;
import com.nyc.work.task.order.verify.ICreateOrderVerify;

import java.util.ArrayList;
import java.util.List;

/**
 * SingleTaskOrderLogic
 *
 * @Description: SingleTaskOrderLogic
 * @Author: nyc
 * @Date: 2023/3/7
 */
public class SingleTaskOrderLogic implements ICreateOrderLogicBuilder {
    private final List<ICreateOrderVerify> iCreateOrderVerifyList = new ArrayList<>();
    private final List<ICreateOrderExtend> iCreateOrderExtendList = new ArrayList<>();
    private ITaskContextBuilder taskDataContextBuilder = null;

    @Override
    public void registerVerifier(ICreateOrderVerify createOrderVerify) {
        iCreateOrderVerifyList.add(createOrderVerify);
    }

    @Override
    public void verify() {
        for (ICreateOrderVerify createOrderVerify : iCreateOrderVerifyList) {
            createOrderVerify.verify();
        }
    }

    @Override
    public void registerExtendLogic(ICreateOrderExtend createOrderExtend) {
        iCreateOrderExtendList.add(createOrderExtend);
    }

    @Override
    public void executeExtendLogic() {
        for (ICreateOrderExtend createOrderExtend : iCreateOrderExtendList) {
            createOrderExtend.extendLogic();
        }
    }

    @Override
    public void setTaskDataContextBuilder(ITaskContextBuilder taskDataContextBuilder) {
        this.taskDataContextBuilder = taskDataContextBuilder;
    }

    @Override
    public void createOrder() {
        verify();
        TaskVo taskVo = taskDataContextBuilder.buildTaskContext();
        executeExtendLogic();
    }
}
