package com.nyc.work.task.order.databuilder;

import com.nyc.work.task.model.TaskVo;

/**
 * IhubTaskDataContextBuilder
 *
 * @Description: IhubTaskDataContextBuilder
 * @Author: nyc
 * @Date: 2023/3/8
 */
public class IhubTaskContextBuilder extends CommonTaskContextBuilder {

    protected TaskVo buildNormalTaskData() {
        TaskVo taskVo = new TaskVo();
        taskVo.setName("IhubTaskDataContextBuilder");
        return taskVo;
    }
}
