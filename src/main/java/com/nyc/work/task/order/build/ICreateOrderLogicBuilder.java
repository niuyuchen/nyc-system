package com.nyc.work.task.order.build;

import com.nyc.work.task.order.databuilder.ITaskContextBuilder;
import com.nyc.work.task.order.extend.ICreateOrderExtend;
import com.nyc.work.task.order.verify.ICreateOrderVerify;

public interface ICreateOrderLogicBuilder {
    void registerVerifier(ICreateOrderVerify createOrderVerify);
    void verify();
    void registerExtendLogic(ICreateOrderExtend createOrderExtend);
    void executeExtendLogic();
    void setTaskDataContextBuilder(ITaskContextBuilder taskDataContextBuilder);
    void createOrder();
}
