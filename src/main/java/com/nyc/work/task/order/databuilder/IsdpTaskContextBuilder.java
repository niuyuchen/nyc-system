package com.nyc.work.task.order.databuilder;

import com.nyc.work.task.model.TaskVo;

/**
 * IsdpTaskDataContextBuilder
 *
 * @Description: IsdpTaskDataContextBuilder
 * @Author: nyc
 * @Date: 2023/3/8
 */
public class IsdpTaskContextBuilder extends CommonTaskContextBuilder {

    protected TaskVo buildNormalTaskData() {
        TaskVo taskVo = new TaskVo();
        taskVo.setName("IsdpTaskDataContextBuilder");
        return taskVo;
    }
}
