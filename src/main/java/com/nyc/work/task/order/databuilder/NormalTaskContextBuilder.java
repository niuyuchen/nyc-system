package com.nyc.work.task.order.databuilder;

import com.nyc.work.task.model.TaskVo;

/**
 * NormalTaskDataContextBuilder
 *
 * @Description: NormalTaskDataContextBuilder
 * @Author: nyc
 * @Date: 2023/3/8
 */
public class NormalTaskContextBuilder extends CommonTaskContextBuilder {

    protected TaskVo buildNormalTaskData() {
        TaskVo taskVo = new TaskVo();
        taskVo.setName("NormalTaskDataContextBuilder");
        return taskVo;
    }
}
