package com.nyc.work.task.service;

import com.nyc.work.task.model.TaskParam;
import com.nyc.work.task.model.TaskVo;
import com.nyc.work.task.order.CreateOrderLogicFactory;

/**
 * TaskOrderMangerService
 *
 * @Description: TaskOrderMangerService
 * @Author: nyc
 * @Date: 2023/3/7
 */
public class TaskOrderMangerService {
    public TaskVo createTaskOrder(TaskParam taskParam) {
        CreateOrderLogicFactory.getCreateOrderLogic(taskParam.getType()).createTaskOrder();
        return null;
    }
}
