package com.nyc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * SystemApplication
 *
 * @Description: SystemApplication
 * @Author: nyc
 * @Date: 2023/3/9
 */
@SpringBootApplication
@EnableConfigurationProperties
        //(exclude= DataSourceAutoConfiguration.class)
public class SystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(SystemApplication.class, args);
    }
}
