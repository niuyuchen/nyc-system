package com.nyc.handler;

import com.nyc.event.EventType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Objects;

/**
 * @author 3y
 * 发送各个渠道的handler
 */
@Slf4j
public abstract class BaseHandler implements Handler {
    @Autowired
    private HandlerHolder handlerHolder;

    /**
     * 标识渠道的Code
     * 子类初始化的时候指定
     */
    protected Integer channelCode;

    /**
     * 子类初始化的时候指定
     */
    protected EventType eventType;

    /**
     * 初始化渠道与Handler的映射关系
     */
    @PostConstruct
    private void init() {
        handlerHolder.putHandler(channelCode, this);
    }

    /**
     * 流量控制
     *
     * @param taskInfo taskInfo
     */
    public void flowControl(TaskInfo taskInfo) {
        // 只有子类指定了限流参数，才需要限流
        if (Objects.nonNull(eventType)) {
            log.info(String.valueOf(eventType));
        }
    }

    @Override
    public void doHandler(TaskInfo taskInfo) {
        flowControl(taskInfo);
        if (handler(taskInfo)) {
            log.info("doHandler.if");
            return;
        }
        log.error("doHandler.if-else");
    }


    /**
     * 统一处理的handler接口
     *
     * @param taskInfo taskInfo
     * @return boolean
     */
    public abstract boolean handler(TaskInfo taskInfo);


}
