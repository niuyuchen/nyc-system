package com.nyc.handler;

import lombok.Getter;
import lombok.Setter;

/**
 * TaskInfo
 *
 * @Description: TaskInfo
 * @Author: nyc
 * @Date: 2023/3/25
 */
@Getter
@Setter
public class TaskInfo {
    private String name;
    private String code;
    private Integer channelTypeCode;
}
