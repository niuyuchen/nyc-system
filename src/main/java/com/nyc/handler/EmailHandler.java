package com.nyc.handler;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * 短信发送处理
 *
 * @author 3y
 */
@Component
@Slf4j
public class EmailHandler extends BaseHandler implements Handler {

    public EmailHandler() {
        channelCode = AUTO_FLOW_RULE;
    }

    /**
     * 流量自动分配策略
     */
    private static final Integer AUTO_FLOW_RULE = 0;

    @Override
    public boolean handler(TaskInfo taskInfo) {
        if (Objects.isNull(taskInfo)) {
            log.error("taskInfo is null");
            return false;
        }
        log.info("taskInfo is {}", JSON.toJSONString(taskInfo));
        return true;
    }

    @Override
    public void recall(TaskInfo taskInfo) {

    }
}
