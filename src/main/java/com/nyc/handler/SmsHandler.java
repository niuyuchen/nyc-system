package com.nyc.handler;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * 短信发送处理
 *
 * @author 3y
 */
@Component
@Slf4j
public class SmsHandler extends BaseHandler implements Handler {

    public SmsHandler() {
        channelCode = ChannelType.SMS.getCode();
    }

    @Override
    public boolean handler(TaskInfo taskInfo) {
        if (Objects.isNull(taskInfo)) {
            log.error("taskInfo is null");
            return false;
        }
        log.info("taskInfo is {}", JSON.toJSONString(taskInfo));
        return true;
    }

    @Override
    public void recall(TaskInfo taskInfo) {

    }
}
